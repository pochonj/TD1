<?php
namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
class RouteurURL
{
    public static function traiterRequete()
    {
        $requete = Request::createFromGlobals();
        $routes = new RouteCollection();

// Route afficherListe
        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("afficherListe", $route);

        $contexteRequete = (new RequestContext())->fromRequest($requete);
        $associateurUrl = new UrlMatcher($routes, $contexteRequete);
        $donneesRoute = $associateurUrl->match($requete->getPathInfo());
        var_dump($donneesRoute);
    }
}